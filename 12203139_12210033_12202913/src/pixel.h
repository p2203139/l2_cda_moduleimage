#ifndef _PIXEL_H
#define _PIXEL_H

/**
* @file pixel.h
* Lib pixel
* @author groupe : TTY
*/

/**
 * @class Pixel
 *
 * @brief La classe Pixel comporte 3 attributs: r,g,b correspondant a la couleur du pixel. Le constructeur sans arguement creera un pixel noir.
 *
 * Une classe pour illustrer l'utilisation de Doxygen pour générer de la documentation.
 */


class Pixel {
	int r,g,b;
	public:
		Pixel(); // init r,g,b a noir
		Pixel(int r, int g, int b);
		void affiche();
};

#endif