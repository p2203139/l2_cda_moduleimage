#include "image.h"
#include <fstream>
#include <iostream>
#include <string>

using namespace std;

/**
* @file image.cpp
* Fichier image
* @author groupe : TTY
*/

/**
 * @brief Constructeur de la class Image, sans arguement.
 * @return Aucune.
 */


Image::Image() {
	this->dimx = 0;
	this->dimy = 0;
}


/**
 * @brief Constructeur de la class Image, avec arguement.
 * 
 * @param dimx longueur de l'image
 * @param dimy hauteur de l'image.
 * @return Aucune.
 */

Image::Image(int dimx, int dimy) {
	this->dimx = dimx;
	this->dimy = dimy;
	this->tab = new Pixel[dimx*dimy];
}

/**
 * @brief Destructeur de la class Image.
 * @return Aucune.
 */

Image::~Image() {
	delete[] this->tab;
	this->dimx = 0;
	this->dimy = 0;
}

/**
 * @brief Renvoie le pixel du tableau 1D.
 * 
 * @param x xieme colonne de l'image.
 * @param y yieme ligne de l'image.
 * @return Le pixel au cordonnee x,y.
 */

Pixel& Image::getPix(int x, int y) {
	assert (x >= 0 && x < this->dimx && y >= 0 && y < this->dimy)
	return this->tab[y*dimx+x]; 
}

 /**
 * @brief Renvoie le pixel du tableau 1D.
 * 
 * @param x xieme colonne de l'image.
 * @param y yieme ligne de l'image.
 * @return Le pixel au cordonnee x,y.
 */

Pixel Image::getPix(int x, int y) const {
	assert (x >= 0 && x < this->dimx && y >= 0 && y < this->dimy)
	return this->tab[y*dimx+x];
}

/**
 * @brief Donne une couleur au pixel donnee.
 * 
 * @param x xieme colonne de l'image.
 * @param y yieme ligne de l'image.
 * @param couleur la couleur a donner.
 * @return Aucune.
 */

void Image::setPix(int x, int y, Pixel couleur) {
	this->tab[y * this->dimx + x] = couleur;
}

/**
 * @brief Dessine un rectangle de couleur <couleur>.
 * 
 * @param xMin coordonnee x du coin bas gauche du rectangle.
 * @param yMin coordonnee y du coin bas gauche du rectangle.
 * @param xMax coordonnee x du coin haut droit du rectangle.
 * @param yMax coordonnee y du coin haut droit du rectangle.
 * @param couleur couleur la coulleur a donner.
 * @return Aucune.
 */

void Image::dessinerRectangle(int xMin, int yMin, int xMax, int yMax, Pixel couleur) {
	for(int x = xMin ; x < xMax ; x++) {
		for(int y = yMin ; y < yMax ; y++)
			this->setPix(x,y,couleur);
	}
}

 /**
 * @brief Efface l'image et lui donne la couleur donnee.
 * @param couleur la nouvelle couleur de l'image.
 * @return Aucune.
 */
void Image::effacer(Pixel couleur) {
	dessinerRectangle(0,0, this->dimx, this->dimy, couleur);
}

/**
 * @brief test le bon fonctionnement des differentes fonctions.
 * @return Aucune.
 */
void Image::testRegression() {
	// TODO	
}

// Fonction du prof

void Image::sauver(const string &filename) const
{
    ofstream fichier(filename.c_str());
    assert(fichier.is_open());
    fichier << "P3" << endl;
    fichier << dimx << " " << dimy << endl;
    fichier << "255" << endl;
    for (unsigned int y = 0; y < dimy; ++y)
        for (unsigned int x = 0; x < dimx; ++x)
        {
            const Pixel &pix = getPix(x, y);
            fichier << +pix.r << " " << +pix.g << " " << +pix.b << " ";
        }
    cout << "Sauvegarde de l'image " << filename << " ... OK\n";
    fichier.close();
}

void Image::ouvrir(const string &filename)
{
    ifstream fichier(filename.c_str());
    assert(fichier.is_open());
    char r, g, b;
    string mot;
    dimx = dimy = 0;
    fichier >> mot >> dimx >> dimy >> mot;
    assert(dimx > 0 && dimy > 0);
    if (tab != nullptr)
        delete[] tab;
    tab = new Pixel[dimx * dimy];
    for (unsigned int y = 0; y < dimy; ++y)
        for (unsigned int x = 0; x < dimx; ++x)
        {
            fichier >> r >> b >> g;
            setPix(x, y, Pixel(r,g,b));
        }
    fichier.close();
    cout << "Lecture de l'image " << filename << " ... OK\n";
}

void Image::afficherConsole()
{
    cout << dimx << " " << dimy << endl;
    for (unsigned int y = 0; y < dimy; ++y)
    {
        for (unsigned int x = 0; x < dimx; ++x)
        {
            const Pixel &pix = getPix(x, y);
            cout << +pix.r << " " << +pix.g << " " << +pix.b << " ";
        }
        cout << endl;
    }
}

