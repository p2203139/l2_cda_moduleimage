#include "image.h"
#include <SDL2/SDL.h>

/**
 * @class ImageViewer
 * 
 * @brief La classe ImageViewer comporte 3 attributs SDL permettant de gerer la texture et l'affichage SDl. La  methode afficher dessine l'image graphique.
 * 
*/

class ImageViewer {
    private:
        SDL_Window * m_window;
        SDL_Renderer * m_renderer;
        SDL_Texture * m_texture;

     public:
        /**
        * @brief Constructeur qui initialiseSDL2 et creer la fenêtre.
        * 
        * */ 
        ImageViewer();

        /**
         * @brief Detruit et ferme SDL2
         * 
         * */
        ~ImageViewer();

        /**
         * @brief Affiche l’image passée en paramètre et permet le (dé)zoom
         * @param im l'image
         * 
         * */
        void afficher (const Image& im);
    };
