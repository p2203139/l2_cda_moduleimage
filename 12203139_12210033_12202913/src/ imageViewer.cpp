#include "imageViewer.h"
#include "image.h"
#include <iostream>
#include <cassert>

using namespace std;


/**
* @brief Constructeur de la classe ImageViewer
* */
ImageViewer::ImageViewer() :m_surface(nullptr), m_texture(nullptr) {
    // initialisation de la sdl
    cout<<"SDL: init"<<endl;
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        cout << "Erreur lors de l'initialisation de la SDL : " << SDL_GetError() << endl;SDL_Quit();exit(1);
    }

    m_renderer = SDL_CreateRenderer(Window, -1, SDL_RENDERER_ACCELERATED);

    // creation de la fenetre et verification de l'ouverture
    m_window = SDL_CreateWindow("Module Image", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, DIMX, DIMY, SDL_WINDOW_SHOWN);
    if (m_window == nullptr) {
        std::cout << "Erreur lors de la creation de la fenetre : " << SDL_GetError() << std::endl;
        SDL_Quit();
        exit(1);
    }
}

/**
* @brief Deconstructeur de la classe ImageViewer
* */
ImageViewer::~ImageViewer(){
    SDL_DestroyTexture(Texture);
    SDL_DestroyRenderer(Renderer);
    SDL_DestroyWindow(Window);
    m_texture = nullptr;
    m_renderer = nullptr;
    m_window = nullptr;
    SDL_Quit();
}

/**
* @brief Methode de la classe ImageViewer pour afficher une image, l'agrandir et retrecir.
* @param im l'image 
* */
void ImageViewer::afficher(const Image& im) {

    int h = im.DIMY;
    int l = im.DIMX;
    int cx = (DIMX - largeur) / 2;
    int cx = (DIMY - hauteur) / 2;

    SDL_Rect r = {0,0,0,0};
    r.cx = 0;
    r.cy= 0;
    r.l = l;
    r.h = h;


    SDL_Event e;
    int quit = 0;
    while (!quit) {
        // Gestion des événements
        while (SDL_PollEvent(&e) != 0) {
            if (e.type == SDL_QUIT || e.key.keysym.sym == SDLK_q) {
                quit = true;
            }
            else if (e.type == SDL_KEYDOWN) {
                // T pour zommer, G pour dezommer
                if (e.key.keysym.sym == SDLK_t) {
                    r.l += 20;
                    r.h += 20;
                    r.x -= 10;
                    r.y -= 10;
                } 
                else if (e.key.keysym.sym == SDLK_g) {
                    r.l-= 20;
                    r.h -= 20;
                    r.x += 10;
                    r.y += 10;
                }
            }
        }
    }
    
    m_window = SDL_CreateWindow("Image", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 200, 200, SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
    m_renderer = SDL_CreateRenderer(m_window, -1,SDL_RENDERER_ACCELERATED);
    m_surface = SDL_CreateRGBSurfaceFrom(tabpixel,200,200,24,200*3,0xff000000,0x00ff0000,0x0000ff00,0x000000ff);

}
