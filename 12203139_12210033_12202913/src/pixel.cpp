#include <iostream>
#include "pixel.h"


/**
* @file pixel.cpp
* Fichier pixel
* @author groupe : TTY
*/

using namespace std;

/**
 * @brief Constructeur de la class Pixel, avec aucun arguement.
 * @return Aucune.
 */

Pixel::Pixel() {
	this->r = 0;
	this->g = 0;
	this->b = 0;
}
/**
 * @brief Constructeur de la class Pixel, avec 2 arguements.
 * 
 * @param r couleur rouge.
 * @param g couleur verte.
 * @param b couleur bleu.
 * @return Aucune.
 */

Pixel::Pixel(int r, int g, int b) {
	this->r = r;
	this->g = g;
	this->b = b;
}

/**
 * @brief affiche les attributs r,g,b de l'instace de pixel actuelle.
 * @return Aucune.
 */

void Pixel::affiche() {
	cout << this->r << ' ' << this->g << ' ' << this->b << '\n';
}
