#include "pixel.h"

#ifndef _IMAGE_H
#define _IMAGE_H

/**
* @file image.h
* Lib image
* @author groupe : TTY
*/

/**
 * @class Image
 *
 * @brief La classe image permet de generer des images de differentes tailles (par defaut 0px), 2 methodes pour recuperer un pixel donner,
 * 1 methode pour modifier un pixel, 1 methode pour dessiner un rectangle d'une couleur donnee et une derniere methode pour effacer toute l'image.
 * les attributs sont stockes dans dimx et dimy, tab fait office de tableau 1D pour l'image.
 */

class Image {
	public:
		Image();
		Image(int dimx, int dimy); // fait verif
		~Image();
		const Pixel& getPix(int x, int y); // renvoie le pixel original
		Pixel getPix(int x, int y) const; // renvoie une copie du pixel
		void setPix(int x, int y, Pixel couleur);
		void dessinerRectangle(int xMin, int yMin, int xMax, int yMax, Pixel couleur);
		void effacer(Pixel couleur);
		static void testRegression();
	private:
		int dimx, dimy;
		Pixel* tab;
};

#endif